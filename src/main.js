import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import VuetifyToast from 'vuetify-toast-snackbar'
import VDateTimePicker from 'vuetify2.x-datetime-picker'


Vue.config.productionTip = false
Vue.use(VuetifyToast, {
	x: 'right', // default
	y: 'top', // default
	color: 'info', // default
	icon: 'info',
	timeout: 3000, // default
	dismissable: true, // default
	autoHeight: false, // default
	multiLine: false, // default
	vertical: false, // default
	shorts: {
		custom: {
			color: 'purple'
		}
	},
	property: '$toast' // default
})

Vue.use(VDateTimePicker)

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
